package xyz.rahmatnasution.event;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xyz.rahmatnasution.event.Api.ApiClient;
import xyz.rahmatnasution.event.Api.ApiInterface;
import xyz.rahmatnasution.event.Helper.UserSession;
import xyz.rahmatnasution.event.Model.Testimony;

public class MenuActivity extends AppCompatActivity implements
        ViewPager.OnPageChangeListener,BottomNavigationView.OnNavigationItemSelectedListener{
    public static final String TAG = MenuActivity.class.getSimpleName();
    MainPagerAdapter mainPagerAdapter;
    ViewPager viewPager;
    private ImageView[] dots;
    LinearLayout pager_indicator;
    BottomNavigationView bottomNavigationView;

    ApiInterface apiInterface;
    List<Testimony> testimonies = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UserSession.getInstance().setActivity(this);
        setContentView(R.layout.activity_menu);
        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.addOnPageChangeListener(this);
        pager_indicator = (LinearLayout) findViewById(R.id.viewPagerCountDots);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        getTestimonies();
    }

    public void company(View view) {
        Intent company = new Intent(this, CompanyProfile.class);
        startActivity(company);
    }

    public void product(View view) {
        Intent company = new Intent(this, ProductActivity.class);
        startActivity(company);
    }
    public void business(View view) {
        Intent company = new Intent(this, BussinesPlane.class);
        startActivity(company);
    }
    public void test(View view) {
        Intent company = new Intent(this, Gallery.class);
        startActivity(company);
    }

    void getTestimonies(){
        apiInterface.getTestimonies().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String response_string = response.body().string();
                    JsonObject jsonObject = (JsonObject) new JsonParser().parse(response_string);
                    JsonArray testimony_array = jsonObject.getAsJsonObject("testimoni").getAsJsonArray("records");
                    for(JsonElement jsonElement : testimony_array){
                        Testimony testimony = new Testimony(
                                jsonElement.getAsJsonArray().get(0).getAsInt(),
                                jsonElement.getAsJsonArray().get(1).getAsString(),
                                jsonElement.getAsJsonArray().get(2).getAsString(),
                                jsonElement.getAsJsonArray().get(3).getAsString(),
                                jsonElement.getAsJsonArray().get(4).getAsString(),
                                jsonElement.getAsJsonArray().get(5).getAsInt()
                                );
                        testimonies.add(testimony);
                    }
                    mainPagerAdapter = new MainPagerAdapter(getSupportFragmentManager(),testimonies.size());
                    viewPager.setAdapter(mainPagerAdapter);
                    setUiPageViewController();
                }catch (Exception e){
                    Log.d(TAG, "onResponse: e = "+e.fillInStackTrace());
                    Log.d(TAG, "onResponse: response = "+response.raw());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG, "onFailure: t = "+t.fillInStackTrace());
            }
        });
    }

    @Override
    public void onPageSelected(int position) {
        for (int i = 0; i < mainPagerAdapter.getCount(); i++) {
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.nonselecteditem_dot));
        }
        dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dot));
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }
    private void setUiPageViewController() {


        dots = new ImageView[mainPagerAdapter.getCount()];

        for (int i = 0; i < mainPagerAdapter.getCount(); i++) {
            dots[i] = new ImageView(this);
            dots[i].setPadding(10,10,10,10);
            dots[i].setImageDrawable(ContextCompat.getDrawable(this,R.drawable.nonselecteditem_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(4, 0, 4, 0);

            pager_indicator.addView(dots[i], params);
        }

        dots[0].setImageDrawable(ContextCompat.getDrawable(this,R.drawable.selecteditem_dot));
    }
    private class MainPagerAdapter extends FragmentStatePagerAdapter {
        private int NUM_PAGES;
        public MainPagerAdapter(FragmentManager fm, int NUM_PAGES) {
            super(fm);
            this.NUM_PAGES = NUM_PAGES;
        }

        @Override
        public Fragment getItem(int position) {
            return TestimonySliderFragment.newInstance(testimonies.get(position));
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case R.id.menu_account:
                UserSession.getInstance().logout();
                startActivity(new Intent(this,LoginActivity.class));
                break;
        }
        return true;
    }

    public void event(View view) {
        Intent event = new Intent(this, EventActivity.class);
        startActivity(event);
    }
}
