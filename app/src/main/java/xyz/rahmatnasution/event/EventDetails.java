package xyz.rahmatnasution.event;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xyz.rahmatnasution.event.Api.ApiClient;
import xyz.rahmatnasution.event.Api.ApiInterface;
import xyz.rahmatnasution.event.Helper.EventHelper;
import xyz.rahmatnasution.event.Model.Event;

public class EventDetails extends AppCompatActivity {
    public static final String TAG = EventDetails.class.getSimpleName();
    int event_id = -1;
    Event event;
    ApiInterface apiInterface;
    CollapsingToolbarLayout collapsingToolbarLayout;

    TextView titleTextView;
    TextView titleDescTextView;
    TextView eventTimeStamp;
    TextView descTextView;
    TextView eventVenue;
    TextView eventPrice;

    ImageView imageHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_details);
        if(getIntent().getExtras() != null){
            event_id = getIntent().getExtras().getInt(EventActivity.EVENT_ID_KEY);
        }
        Toolbar toolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        titleTextView = (TextView) findViewById(R.id.event_title);
        titleDescTextView = (TextView) findViewById(R.id.event_title_desc);
        descTextView = (TextView) findViewById(R.id.event_description);
        eventTimeStamp = (TextView) findViewById(R.id.event_time_stamp);
        eventVenue = (TextView) findViewById(R.id.event_venue);
        eventPrice = (TextView) findViewById(R.id.event_price);
        imageHeader = (ImageView) findViewById(R.id.image_header);
        collapsingToolbarLayout = (CollapsingToolbarLayout)findViewById(R.id.collapse_toolbar);
        collapsingToolbarLayout.setTitle("Event Details");
        collapsingToolbarLayout.setExpandedTitleColor(Color.TRANSPARENT);

        Context context = this;
        collapsingToolbarLayout.setContentScrimColor(ContextCompat.getColor(context,R.color.colorAccent));
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        getEventDetails();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void tickets(View view) {
        if(event == null){
            Snackbar.make(collapsingToolbarLayout,"Event is not found!", Snackbar.LENGTH_SHORT).show();
        }else {

            Intent ticket = new Intent(this, TicketDetails.class);
            ticket.putExtra("event",new Gson().toJson(event));
            startActivity(ticket);
        }
    }

    void getEventDetails(){
        apiInterface.getEventDetails(String.valueOf(event_id)).enqueue(new Callback<Event>() {
            @Override
            public void onResponse(Call<Event> call, Response<Event> response) {
                if(response.isSuccessful()){
                    event = response.body();
                    writeEvent();
                }else {
                    Snackbar.make(collapsingToolbarLayout,"Something Error", Snackbar.LENGTH_LONG)
                            .show();
                    Log.d(TAG, "onResponse: "+response.raw());
                }

            }

            @Override
            public void onFailure(Call<Event> call, Throwable t) {

                Snackbar.make(collapsingToolbarLayout,"Failed to get event data", Snackbar.LENGTH_LONG)
                        .setAction("Try Again", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                getEventDetails();
                            }
                        })
                        .show();
                Log.d(TAG, "onFailure: t = "+t.fillInStackTrace());
            }
        });
    }

    public void writeEvent(){
        collapsingToolbarLayout.setTitle(event.getTitle());
        titleTextView.setText(event.getTitle());
        titleDescTextView.setText(event.getTitle_description());
        eventTimeStamp.setText(EventHelper.getScheduleFromTimeStamp(event.getFrom_timestamp(),event.getTo_timestamp()));
        eventPrice.setText(event.getPrice());
        eventVenue.setText(event.getVenue());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            descTextView.setText(Html.fromHtml(event.getDescription(),Html.FROM_HTML_MODE_LEGACY));
        } else {
            descTextView.setText(Html.fromHtml(event.getDescription()));
        }
        Glide.with(this).load(event.getImageURL())
                .into(imageHeader);
    }
}
