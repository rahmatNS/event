package xyz.rahmatnasution.event;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xyz.rahmatnasution.event.Api.ApiClient;
import xyz.rahmatnasution.event.Api.ApiInterface;
import xyz.rahmatnasution.event.Helper.EventHelper;
import xyz.rahmatnasution.event.Helper.UserSession;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    public static final String TAG = LoginActivity.class.getSimpleName();
    AppCompatEditText nameEditText;
    AppCompatEditText emailEditText;
    AppCompatEditText passwordEditText;
    AppCompatEditText password2EditText;
    Button btnLoginAsMember;
    Button btnLoginAsSpeaker;
    Button btnRegister;
    TextView signUpTextView;
    TextView signInTextView;
    ProgressDialog progressDialog;
    ApiInterface apiInterface;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UserSession.getInstance().setActivity(this);
        setContentView(R.layout.activity_login);
        btnLoginAsMember = (Button) findViewById(R.id.login_as_member);
        btnLoginAsMember.setOnClickListener(this);
        btnLoginAsSpeaker = (Button) findViewById(R.id.login_as_speaker);
        btnLoginAsSpeaker.setOnClickListener(this);
        btnRegister = (Button) findViewById(R.id.register);
        btnRegister.setOnClickListener(this);
        nameEditText = (AppCompatEditText) findViewById(R.id.name);
        emailEditText = (AppCompatEditText) findViewById(R.id.email);
        passwordEditText = (AppCompatEditText) findViewById(R.id.password);
        password2EditText = (AppCompatEditText) findViewById(R.id.password2);
        password2EditText.addTextChangedListener(password2TextwWatcher);
        signUpTextView = (TextView) findViewById(R.id.sign_up_text_view);
        signUpTextView.setOnClickListener(this);
        signInTextView = (TextView) findViewById(R.id.sign_in_text_view);
        signInTextView.setOnClickListener(this);
        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setIndeterminate(true);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        setupForm(true);
    }

    public void speaker (View view){
        Intent formevent = new Intent(this, FormEvent.class);
        startActivity(formevent);
    }

    public void doLogin (){
        if(emailEditText.getText().length() == 0){
            emailEditText.setError("email is required");
            return;
        }else if(!EventHelper.isValidEmail(emailEditText.getText().toString())){
            emailEditText.setError("email is not valid");
            return;
        }
        if(passwordEditText.getText().length() == 0){
            passwordEditText.setError("password is required");
            return;
        }
        progressDialog.setMessage("Signing in..");
        progressDialog.show();

        Call<ResponseBody> responseBodyCallback = apiInterface.login(emailEditText.getText().toString(),passwordEditText.getText().toString());
        responseBodyCallback.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressDialog.hide();
                try {
                    String response_string = response.body().string();
                    JsonObject jsonObject = (JsonObject) new JsonParser().parse(response_string);
                    if(jsonObject.get("error").getAsBoolean()){
                        Snackbar.make(btnLoginAsMember,jsonObject.get("error_msg").getAsString(), Snackbar.LENGTH_LONG)
                                .show();
                    }else {
                        JsonObject user = jsonObject.get("user").getAsJsonObject();
                        String name = user.get("name").getAsString();
                        String email = user.get("email").getAsString();
                        UserSession.getInstance().createUserLogin(name,email);
                        checkLogin();
                    }
                }catch (Exception e){
                    Snackbar.make(btnLoginAsMember,"Something Error", Snackbar.LENGTH_LONG)
                            .show();
                    Log.d(TAG, "onResponse: e = "+e.fillInStackTrace());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.hide();
                Snackbar.make(btnLoginAsMember,"Failed to login", Snackbar.LENGTH_LONG)
                        .setAction("Try Again", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                doLogin();
                            }
                        })
                        .show();
                Log.d(TAG, "onFailure: t = "+t.fillInStackTrace());
            }
        });
    }

    public void doSignUp () {
        if (nameEditText.getText().length() == 0) {
            nameEditText.setError("name is required");
            return;
        }
        if (emailEditText.getText().length() == 0) {
            emailEditText.setError("email is required");
            return;
        } else if (!EventHelper.isValidEmail(emailEditText.getText().toString())) {
            emailEditText.setError("email is not valid");
            return;
        }
        if (passwordEditText.getText().length() == 0) {
            passwordEditText.setError("password is required");
            return;
        }
        if (password2EditText.getText().length() == 0) {
            password2EditText.setError("retype password is required");
            return;
        }
        if (!password2EditText.getText().toString().equals(passwordEditText.getText().toString())) {
                password2EditText.setError("text must match with password");
            return;
        }

        progressDialog.setMessage("Signing up..");
        progressDialog.show();

        Call<ResponseBody> responseBodyCallback = apiInterface.register(nameEditText.getText().toString(),emailEditText.getText().toString(),passwordEditText.getText().toString());
        responseBodyCallback.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressDialog.hide();
                try {
                    String response_string = response.body().string();
                    JsonObject jsonObject = (JsonObject) new JsonParser().parse(response_string);
                    if(jsonObject.get("error").getAsBoolean()){
                        Snackbar.make(btnLoginAsMember,jsonObject.get("error_msg").getAsString(), Snackbar.LENGTH_LONG)
                                .show();
                    }else {
                        Log.d(TAG, "onResponse: response_string = "+response_string);
                        JsonObject user = jsonObject.get("user").getAsJsonObject();
                        String name = user.get("name").getAsString();
                        String email = user.get("email").getAsString();
                        UserSession.getInstance().createUserLogin(name,email);
                        checkLogin();
                    }
                }catch (Exception e){
                    Snackbar.make(btnLoginAsMember,"Something Error", Snackbar.LENGTH_LONG)
                            .show();
                    Log.d(TAG, "onResponse: e = "+e.fillInStackTrace());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.hide();
                Snackbar.make(btnLoginAsMember,"Failed to login", Snackbar.LENGTH_LONG)
                        .setAction("Try Again", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                doLogin();
                            }
                        })
                        .show();
                Log.d(TAG, "onFailure: t = "+t.fillInStackTrace());
            }
        });
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.login_as_member:
                doLogin();
                break;
            case R.id.register:
                doSignUp();
                break;
            case R.id.sign_up_text_view:
                setupForm(false);
                break;
            case R.id.sign_in_text_view:
                setupForm(true);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkLogin();
    }

    void checkLogin(){
        if(UserSession.getInstance().isLoggedIn()){
            String name = UserSession.getInstance().getName();
            Toast.makeText(LoginActivity.this, "Welcome "+EventHelper.capitalize(name,null), Toast.LENGTH_SHORT).show();
            Intent menu = new Intent(LoginActivity.this, MenuActivity.class);
            startActivity(menu);
        }
    }
    void setupForm(boolean isLoginForm){
//        only visible on register form
        int registerFormVisibility = isLoginForm ? View.GONE : View.VISIBLE;
        nameEditText.setVisibility(registerFormVisibility);
        password2EditText.setVisibility(registerFormVisibility);
        btnRegister.setVisibility(registerFormVisibility);
        signInTextView.setVisibility(registerFormVisibility);

//        only visible on login form
        int loginFormVisibility = isLoginForm ? View.VISIBLE : View.GONE;
        btnLoginAsMember.setVisibility(loginFormVisibility);
        signUpTextView.setVisibility(loginFormVisibility);
        btnLoginAsSpeaker.setVisibility(loginFormVisibility);
        findViewById(R.id.or).setVisibility(loginFormVisibility);
        findViewById(R.id.forgotpassword).setVisibility(loginFormVisibility);
        findViewById(R.id.sign).setVisibility(loginFormVisibility);

    }

    TextWatcher password2TextwWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if(s.length() > 0 && passwordEditText.getText().length() > 0
                    && !s.toString().equals(passwordEditText.getText().toString())){
                password2EditText.setError("text must match with password");
            }else {
                password2EditText.setError(null);
            }
        }
    };
}
