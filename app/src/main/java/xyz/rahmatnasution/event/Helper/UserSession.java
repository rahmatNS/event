package xyz.rahmatnasution.event.Helper;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by rahma on 30/09/2017.
 */
public class UserSession {
    Activity activity;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    public static final String KEY_NAME = "name";
    public static final String KEY_EMAIL = "email";

    private static UserSession ourInstance = new UserSession();

    public static UserSession getInstance() {
        return ourInstance;
    }

    private UserSession() {
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
        this.sharedPreferences = activity.getSharedPreferences("credentials",Context.MODE_PRIVATE);
        this.editor = sharedPreferences.edit();
    }

    public boolean isLoggedIn(){
        return sharedPreferences.getString("email","").length() > 0;
    }
    public void logout(){
        editor.clear();
        editor.commit();
    }
    public void createUserLogin(String name, String email){
        editor.putString(KEY_NAME,name);
        editor.putString(KEY_EMAIL,email);
        editor.commit();
    }

    public Activity getActivity() {
        return activity;
    }

    public SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

    public SharedPreferences.Editor getEditor() {
        return editor;
    }

    public String getName(){
        return sharedPreferences.getString(KEY_NAME,"");
    }

    public String getEmail(){
        return sharedPreferences.getString(KEY_EMAIL,"");
    }
}
