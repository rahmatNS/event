package xyz.rahmatnasution.event.Helper;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by rahma on 19/09/2017.
 */

public class EventHelper {
    public static final String IMAGE_HOST = "http://139.59.114.166/event/admin/assets/uploads/";

    private EventHelper(){}

    public static String getImageUrl(String filename){
        return IMAGE_HOST+filename;
    }
    public static boolean isValidEmail(String email){
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
    public static String capitalize(String str, char[] delimiters) {
        int delimLen = (delimiters == null ? -1 : delimiters.length);
        if (str == null || str.length() == 0 || delimLen == 0) {
            return str;
        }
        int strLen = str.length();
        StringBuffer stringBuffer = new StringBuffer(strLen);
        boolean capitalizeNext = true;
        for (int i = 0; i < strLen; i++) {
            char ch = str.charAt(i);

            if (isDelimiter(ch, delimiters)) {
                stringBuffer.append(ch);
                capitalizeNext = true;
            } else if (capitalizeNext) {
                stringBuffer.append(Character.toTitleCase(ch));
                capitalizeNext = false;
            } else {
                stringBuffer.append(ch);
            }
        }
        return stringBuffer.toString();
    }

    private static boolean isDelimiter(char ch, char[] delimiters) {
        if (delimiters == null) {
            return Character.isWhitespace(ch);
        }
        for (int i = 0, isize = delimiters.length; i < isize; i++) {
            if (ch == delimiters[i]) {
                return true;
            }
        }
        return false;
    }
    public static String getScheduleFromTimeStamp(String from, String to){
        Long from_timestamp = Long.parseLong(from);
        Long to_timestamp = Long.parseLong(to);
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy 'at' h:mm a");
        return dateFormat.format(new Date(from_timestamp * 1000)) + " - "+dateFormat.format(new Date(to_timestamp * 1000));

    }
}
