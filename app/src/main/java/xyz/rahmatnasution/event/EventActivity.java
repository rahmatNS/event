package xyz.rahmatnasution.event;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xyz.rahmatnasution.event.Api.ApiClient;
import xyz.rahmatnasution.event.Api.ApiInterface;
import xyz.rahmatnasution.event.Helper.EventHelper;
import xyz.rahmatnasution.event.Helper.UserSession;

public class EventActivity extends AppCompatActivity {

    public static final String TAG = EventActivity.class.getSimpleName();
    public static final String EVENT_ID_KEY = "event_id";
    ApiInterface apiInterface;
    CollapsingToolbarLayout collapsingToolbarLayout;

    EventsAdapter eventsAdapter;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        Toolbar toolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        collapsingToolbarLayout = (CollapsingToolbarLayout)findViewById(R.id.collapse_toolbar);
        collapsingToolbarLayout.setTitle("Event");
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        eventsAdapter = new EventsAdapter(new ArrayList<HashMap>());
        recyclerView.setAdapter(eventsAdapter);
//        Context context = this;
//        collapsingToolbarLayout.setContentScrimColor(ContextCompat.getColor(context,R.color.colorAccent));
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        getEvents();

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            this.finish();
            ;
        }
        return super.onOptionsItemSelected(item);
    }

    void getEvents(){
        apiInterface.getEvents().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String response_string = response.body().string();
                    JsonObject jsonObject = (JsonObject) new JsonParser().parse(response_string);
                    JsonObject eventObject = jsonObject.get("event").getAsJsonObject();
                    JsonArray columnArray = eventObject.get("columns").getAsJsonArray();
                    JsonArray recordsArray = eventObject.get("records").getAsJsonArray();
                    List<HashMap<String,String>> hashMaps = new ArrayList();
                    for(JsonElement jsonElement : recordsArray){
                        HashMap<String,String> hashMap = new HashMap();
                        for (int i = 0; i < columnArray.size(); i++){
                            hashMap.put(columnArray.get(i).getAsString(),
                                    jsonElement.getAsJsonArray().get(i).getAsString());
                        }
                        hashMaps.add(hashMap);
                    }
                    eventsAdapter.swap(hashMaps);
                    Log.d(TAG, "onResponse: response_string = "+response_string);
                }catch (Exception e){
                    Snackbar.make(collapsingToolbarLayout,"Something Error", Snackbar.LENGTH_LONG)
                            .show();
                    Log.d(TAG, "onResponse: e = "+e.fillInStackTrace());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                Snackbar.make(collapsingToolbarLayout,"Failed to get events", Snackbar.LENGTH_LONG)
                        .setAction("Try Again", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                getEvents();
                            }
                        })
                        .show();
                Log.d(TAG, "onFailure: t = "+t.fillInStackTrace());
            }
        });
    }
    private class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.ViewHolder>{
        private List<HashMap> eventList;

        EventsAdapter(List<HashMap> hashMaps){
            this.eventList = hashMaps;
        }
        public class ViewHolder extends RecyclerView.ViewHolder{
            ImageView imageView;
            public ViewHolder(View view){
                super(view);
                imageView = (ImageView) view.findViewById(R.id.image);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String id_string = eventList.get(getAdapterPosition()).get("id").toString();
                        int id = Integer.valueOf(id_string);
                        onItemClick(id);
                    }
                });
            }
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            HashMap<String,String> hashMap = eventList.get(position);
            Glide.with(EventActivity.this)
                    .load(EventHelper.getImageUrl(hashMap.get("image")))
                    .into(holder.imageView);
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(EventActivity.this).inflate(R.layout.events_row_item,parent,false);
            return new ViewHolder(view);
        }

        @Override
        public int getItemCount() {
            return eventList == null ? 0 : eventList.size();
        }

        public void swap(List<HashMap<String,String>> hashMaps){
            eventList.clear();
            eventList.addAll(hashMaps);
            notifyDataSetChanged();
        }
    }
    void onItemClick(int id){
        Intent eventDetails = new Intent(this,EventDetails.class);
        Bundle bundle = new Bundle();
        bundle.putInt(EVENT_ID_KEY,id);
        eventDetails.putExtras(bundle);
        startActivity(eventDetails);
    }
}
