package xyz.rahmatnasution.event;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.google.gson.Gson;

import xyz.rahmatnasution.event.Helper.EventHelper;
import xyz.rahmatnasution.event.Model.Event;

public class TicketDetails extends AppCompatActivity implements View.OnClickListener{
    public static final String TAG = TicketDetails.class.getSimpleName();
    Event event;
    TextView titleTextView;
    TextView titleDescTextView;
    TextView eventScheduleTextView;
    TextView quantityPicker;
    AlertDialog quantityDialog;
    NumberPicker quantityNumberPicker;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_details);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(getIntent().getExtras() != null){
            String event_string = getIntent().getExtras().getString("event");
            event = new Gson().fromJson(event_string,Event.class);
        }

        getSupportActionBar().setTitle("Ticket Details");

        //add button activity

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        titleTextView = (TextView) findViewById(R.id.event_title);
        titleDescTextView = (TextView) findViewById(R.id.event_title_desc);
        eventScheduleTextView = (TextView) findViewById(R.id.event_schedule);
        quantityPicker = (TextView) findViewById(R.id.qty_value);
        quantityPicker.setOnClickListener(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select number of ticket(s)");
        quantityNumberPicker = new NumberPicker(this);
        quantityNumberPicker.setMinValue(1);
        quantityNumberPicker.setMaxValue(Integer.MAX_VALUE);
        builder.setView(quantityNumberPicker);
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                quantityPicker.setText(String.valueOf(quantityNumberPicker.getValue()));
            }
        });
        quantityDialog = builder.create();
        writeEventData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            this.finish();
            ;
        }
        return super.onOptionsItemSelected(item);
    }
    public void register(View view) {
        Intent company = new Intent(this, Register.class);
        startActivity(company);
    }
    void writeEventData(){
        titleTextView.setText(event.getTitle());
        titleDescTextView.setText(event.getTitle_description());
        eventScheduleTextView.setText(EventHelper.getScheduleFromTimeStamp(event.getFrom_timestamp(),event.getTo_timestamp()));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.qty_value:
                quantityDialog.show();
                break;
        }
    }
}
