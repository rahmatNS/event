package xyz.rahmatnasution.event;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.HashMap;
import java.util.List;

import xyz.rahmatnasution.event.Helper.EventHelper;
import xyz.rahmatnasution.event.Model.Event;

public class ProductActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
    }

    private class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder>{
        List<HashMap<String,String>> hashMaps;
        class ViewHolder extends RecyclerView.ViewHolder{
            ImageView imageView;
            public ViewHolder(View view){
                super(view);
                imageView = (ImageView) view.findViewById(R.id.image);
            }
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            Glide.with(ProductActivity.this).load(EventHelper.getImageUrl(hashMaps.get(position).get("image")))
                    .into(holder.imageView);
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(ProductActivity.this).inflate(R.layout.events_row_item,parent,false);
            return new ViewHolder(view);
        }

        @Override
        public int getItemCount() {
            return hashMaps == null ? 0 : hashMaps.size();
        }
    }
}
