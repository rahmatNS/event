package xyz.rahmatnasution.event.Api;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import xyz.rahmatnasution.event.Model.Event;
import xyz.rahmatnasution.event.Model.Testimony;
import xyz.rahmatnasution.event.Model.TestimonyResult;

/**
 * Created by rahma on 02/01/2017.
 */

public interface ApiInterface {
    @FormUrlEncoded
    @POST("event/loginapi/register.php")
    Call<ResponseBody> register(@Field("name") String name, @Field("email") String email, @Field("password") String password);

    @FormUrlEncoded
    @POST("event/loginapi/login.php")
    Call<ResponseBody> login(@Field("email") String email, @Field("password") String password);

    @GET("event/api.php/company_profile")
    Call<ResponseBody> getCompanyProfile();
    @GET("event/api.php/testimoni")
    Call<ResponseBody> getTestimonies();


    @GET("event/api.php/event")
    Call<ResponseBody> getEvents();

    @GET("event/api.php/event/{id}")
    Call<Event> getEventDetails(@Path("id") String id);

    @GET("event/api.php/product}")
    Call<ResponseBody> getProducts();
//    @GET("user/category/{category}/search/{search}/offset/{offset}/{login}/{access_token}")
//    Call<ArrayList<User>> searchUserByCateg(@Path("category") String category, @Path("search") String search, @Path("offset") int offset,
//                                            @Path("login") String login, @Path("access_token") String access_token);
//    @GET("user/uid/{uid}/{login}/{access_token}")
//    Call<User> getUserById(@Path("uid") int uid, @Path("login") String login,
//                           @Path("access_token") String access_token);
//    @GET("word_req/search/{search}/offset/{offset}/{login}/{access_token}")
//    Call<List<Word>> searchWordReq(@Path("search") String search, @Path("offset") int offset, @Path("login") String login,
//                                   @Path("access_token") String access_token);
//    @GET("word_req/offset/{offset}/{login}/{access_token}")
//    Call<List<Word>> getWordReq(@Path("offset") int offset, @Path("login") String login,
//                                @Path("access_token") String access_token);
//    @GET("word_req/uid/offset/{offset}/{login}/{access_token}")
//    Call<List<Word>> getWordReqByMe(@Path("offset") int offset, @Path("login") String login,
//                                    @Path("access_token") String access_token);
//    @GET("word_req/uid/search/{search}/offset/{offset}/{login}/{access_token}")
//    Call<List<Word>> searchWordReqByMe(@Path("search") String search, @Path("offset") int offset, @Path("login") String login,
//                                       @Path("access_token") String access_token);
//    @GET("word_req/uid/{uid}/offset/{offset}/{login}/{access_token}")
//    Call<List<Word>> getWordReqByUid(@Path("uid") int uid, @Path("offset") int offset, @Path("login") String login,
//                                     @Path("access_token") String access_token);
//    @GET("word_req/exclude_uid/{uid}/offset/{offset}/{login}/{access_token}")
//    Call<List<Word>> getWordReqExcUid(@Path("uid") int uid, @Path("offset") int offset, @Path("login") String login,
//                                      @Path("access_token") String access_token);
//    @GET("word_req/exclude_uid/offset/{offset}/{login}/{access_token}")
//    Call<List<Word>> getWordReqExcMe(@Path("offset") int offset, @Path("login") String login,
//                                     @Path("access_token") String access_token);
//    @GET("word_req/exclude_uid/search/{search}/offset/{offset}/{login}/{access_token}")
//    Call<List<Word>> searchWordReqExcMe(@Path("search") String search, @Path("offset") int offset, @Path("login") String login,
//                                        @Path("access_token") String access_token);
//    @GET("word_req/question_for_user_id/{uid}/offset/{offset}/{login}/{access_token}")
//    Call<List<Word>> getRequestFor(@Path("uid") int uid, @Path("offset") int offset, @Path("login") String login,
//                                   @Path("access_token") String access_token);
//    @GET("word_req/question_for_user_id/{uid}/search/{search}/offset/{offset}/{login}/{access_token}")
//    Call<List<Word>> searchRequestFor(@Path("uid") int uid, @Path("search") String search, @Path("offset") int offset, @Path("login") String login,
//                                      @Path("access_token") String access_token);
//    @GET("answer_req/{login}/{access_token}")
//    Call<Integer> answerReq(@Path("login") String login,
//                            @Path("access_token") String access_token);
//    @Headers( "Content-Type: application/json-rpc" )
//    @POST("word/new/{login}/{access_token}")
//    Call<ResponseBody> create_vent(@Path("login") String login,
//                                   @Path("access_token") String access_token, @Body JsonObject body);
//    @Headers( {"Content-Type: application/json-rpc","Connection:close"} )
//    @POST("word/new/{login}/{access_token}")
//    Call<ResponseBody> create_word(@Path("login") String login,
//                                   @Path("access_token") String access_token, @Body JsonObject body);
//    @GET("word/activate/{login}/{access_token}")
//    Call<Integer> activate_word(@Path("login") String login,
//                                @Path("access_token") String access_token);
//    @GET("word/deactivate/{login}/{access_token}")
//    Call<Integer> deactivate_word(@Path("login") String login,
//                                  @Path("access_token") String access_token);
//    @Headers( {"Content-Type: application/json-rpc","Connection:close"} )
//    @POST("own_list/new/{login}/{access_token}")
//    Call<ResponseBody> create_ownList(@Path("login") String login,
//                                      @Path("access_token") String access_token, @Body JsonObject body);
//    @GET("own_list/view/{own_list_id}/{login}/{access_token}")
//    Call<List<OwnList>> viewOwnListById(@Path("own_list_id") int own_list_id, @Path("login") String login,
//                                        @Path("access_token") String access_token);
//    @GET("own_list/uid/{uid}/offset/{offset}/{login}/{access_token}")
//    Call<List<OwnList>> getOwnListByUid(@Path("uid") int uid, @Path("offset") int offset, @Path("login") String login,
//                                        @Path("access_token") String access_token);
//    @GET("own_list/offset/{offset}/{login}/{access_token}")
//    Call<List<OwnList>> getMyOwnList(@Path("offset") int offset, @Path("login") String login,
//                                     @Path("access_token") String access_token);
//    @GET("own_list/search/{search}/offset/{offset}/{login}/{access_token}")
//    Call<List<OwnList>> searchMyOwnList(@Path("search") String search, @Path("offset") int offset, @Path("login") String login,
//                                        @Path("access_token") String access_token);
//    @GET("own_list/my_own_list/{own_id}/{login}/{access_token}")
//    Call<List<OwnList>> getFeedOwnlistById(@Path("own_id") int own_id, @Path("login") String login,
//                                           @Path("access_token") String access_token);
//    @GET("own_list/set_gold/{login}/{access_token}")
//    Call<Integer> setGold(@Path("login") String login,
//                          @Path("access_token") String access_token);
//    @GET("own_list/unset_gold/{login}/{access_token}")
//    Call<Integer> unsetGold(@Path("login") String login,
//                            @Path("access_token") String access_token);
//    @GET("own_list/is_goldwords/offset/{offset}/{login}/{access_token}")
//    Call<List<OwnList>> getGoldwords(@Path("offset") int offset, @Path("login") String login,
//                                     @Path("access_token") String access_token);
//    @GET("own_list/search/{search}/is_goldwords/offset/{offset}/{login}/{access_token}")
//    Call<List<OwnList>> searchGoldwords(@Path("search") String search, @Path("offset") int offset, @Path("login") String login,
//                                        @Path("access_token") String access_token);
//    @GET("own_list/feed/offset/{offset}/{login}/{access_token}")
//    Call<List<OwnList>> getFeed(@Path("offset") int offset, @Path("login") String login,
//                                @Path("access_token") String access_token);
//    @GET("/own_list/feed_feature/offset/{offset}/{login}/{access_token}")
//    Call<List<OwnList>> getFeedFeature(@Path("offset") int offset, @Path("login") String login,
//                                       @Path("access_token") String access_token);
//    @GET("own_list/feed_feature/search/{search}/offset/{offset}/{login}/{access_token}")
//    Call<List<OwnList>> searchFeedFeature(@Path("search") String search, @Path("offset") int offset, @Path("login") String login,
//                                          @Path("access_token") String access_token);
//    @GET("/own_list/feed_following/offset/{offset}/{login}/{access_token}")
//    Call<List<OwnList>> getFeedFollowing(@Path("offset") int offset, @Path("login") String login,
//                                         @Path("access_token") String access_token);
//    @GET("own_list/feed_following/search/{search}/offset/{offset}/{login}/{access_token}")
//    Call<List<OwnList>> searchFeedFollowing(@Path("search") String search, @Path("offset") int offset, @Path("login") String login,
//                                            @Path("access_token") String access_token);
//    @GET("own_list/feed/search/{search}/offset/{offset}/{login}/{access_token}")
//    Call<List<OwnList>> searchFeed(@Path("search") String search, @Path("offset") int offset, @Path("login") String login,
//                                   @Path("access_token") String access_token);
//    @GET("set_fcm_token/{fcm_token}/{login}/{access_token}")
//    Call<ResponseBody> set_fcm_token(@Path("fcm_token") String fcm_token, @Path("login") String login,
//                                     @Path("access_token") String access_token);
//    @GET("logout/{login}/{access_token}")
//    Call<ResponseBody> logout(@Path("login") String login,
//                              @Path("access_token") String access_token);
//    @GET("auth/get_tokens")
//    Call<SessionInfo> getTokens(
//            @Query("state") String state,
//            @Query("access_token") String access_token,
//            @Query("expires_in") int expires_in);
//    @GET("auth/get_tokens")
//    Call<SessionInfo> getTokensWithLogin(
//            @Query("state") String state,
//            @Query("access_token") String access_token,
//            @Query("login") String login,
//            @Query("expires_in") int expires_in);
//    @GET("auth/get_tokens")
//    Call<JsonObject> getTwTokensWithLogin(
//            @Query("state") String state,
//            @Query("access_token") String access_token,
//            @Query("access_token_secret") String access_token_secret,
//            @Query("login") String login,
//            @Query("expires_in") int expires_in);
//    @GET("auth_list/provider_id/{provider_id}/{login}/{access_token}")
//    Call<List<AuthProvider>> get_auth_list_by_provider_id(
//            @Path("provider_id") int provider_id,
//            @Path("login") String login,
//            @Path("access_token") String access_token);
//    @GET("notification/offset/{offset}/{login}/{access_token}")
//    Call<List<Notification>> get_notification(
//            @Path("offset") int offset,
//            @Path("login") String login,
//            @Path("access_token") String access_token);
//    @GET("notification/type/{type}/offset/{offset}/{login}/{access_token}")
//    Call<List<Notification>> get_notification_by_type(
//            @Path("type") String type,
//            @Path("offset") int offset,
//            @Path("login") String login,
//            @Path("access_token") String access_token);
//    @GET("notification/uid/{uid}/offset/{offset}/{login}/{access_token}")
//    Call<List<Notification>> get_user_notification(
//            @Path("uid") int uid,
//            @Path("offset") int offset,
//            @Path("login") String login,
//            @Path("access_token") String access_token);
//    @GET("notification/uid/{uid}/type/{type}/offset/{offset}/{login}/{access_token}")
//    Call<List<Notification>> get_user_notification_by_type(
//            @Path("uid") int uid,
//            @Path("type") String type,
//            @Path("offset") int offset,
//            @Path("login") String login,
//            @Path("access_token") String access_token);
//    @Headers( "Content-Type: application/json-rpc" )
//    @POST("report/new/{login}/{access_token}")
//    Call<ResponseBody> send_report(
//            @Path("login") String login,
//            @Path("access_token") String access_token,
//            @Body JsonObject body);
//    @Headers( "Content-Type: application/json-rpc" )
//    @POST("user/set_data/{login}/{access_token}")
//    Call<ResponseBody> set_data_user(
//            @Path("login") String login,
//            @Path("access_token") String access_token,
//            @Body JsonObject body);
//    @GET("testimony/offset/{offset}/{login}/{access_token}")
//    Call<List<Testimony>> getTestimony(@Path("offset") int offset, @Path("login") String login,
//                                       @Path("access_token") String access_token);
//    @GET("testimony/uid/{uid}/offset/{offset}/{login}/{access_token}")
//    Call<List<Testimony>> getTestimonyForId(@Path("uid") int uid, @Path("offset") int offset, @Path("login") String login,
//                                            @Path("access_token") String access_token);
//    @Headers("Connection: close")
//    @GET("user_feature/offset/{offset}/{login}/{access_token}")
//    Call<List<User>> getUserFeature(@Path("offset") int offset, @Path("login") String login,
//                                    @Path("access_token") String access_token);
//    @Headers( "Content-Type: application/json-rpc" )
//    @POST("user/set_title/{login}/{access_token}")
//    Call<ResponseBody> updateTitleProfile(
//            @Path("login") String login,
//            @Path("access_token") String access_token,
//            @Body JsonObject body);
//    @Headers({ "Content-Type: application/json-rpc" ,"Connection:close"})
//    @POST("user/set_category/{login}/{access_token}")
//    Call<ResponseBody> updateCategoryProfile(
//            @Path("login") String login,
//            @Path("access_token") String access_token,
//            @Body JsonObject body);
//    @Headers({ "Content-Type: application/json-rpc" ,"Connection:close"})
//    @POST("user/set_rate/{login}/{access_token}")
//    Call<ResponseBody> updateRateProfile(
//            @Path("login") String login,
//            @Path("access_token") String access_token,
//            @Body JsonObject body);
//    @Headers({ "Content-Type: application/json-rpc" ,"Connection:close"})
//    @POST("user/set_description/{login}/{access_token}")
//    Call<ResponseBody> updateDescriptionProfile(
//            @Path("login") String login,
//            @Path("access_token") String access_token,
//            @Body JsonObject body);
//    @Multipart
//    @POST("user/set_cover/{login}/{access_token}")
//    Call<ResponseBody> updateCoverProfile(
//            @Path("login") String login,
//            @Path("access_token") String access_token,
//            @Part MultipartBody.Part image);
//    @Multipart
//    @POST("user/set_image/{login}/{access_token}")
//    Call<ResponseBody> updateImageProfile(
//            @Path("login") String login,
//            @Path("access_token") String access_token,
//            @Part MultipartBody.Part image);
//    @GET("bank/offset/{offset}/{login}/{access_token}")
//    Call<ArrayList<Bank>> getBank(@Path("offset") int offset, @Path("login") String login,
//                                  @Path("access_token") String access_token);
//    @GET("bank/search/{search}/offset/{offset}/{login}/{access_token}")
//    Call<ArrayList<Bank>> searchBank(@Path("search") String search, @Path("offset") int offset, @Path("login") String login,
//                                     @Path("access_token") String access_token);
//    @GET("own_list/my_answer/offset/{offset}/{login}/{access_token}")
//    Call<ArrayList<OwnList>> getMyAnswers(@Path("offset") int offset, @Path("login") String login,
//                                          @Path("access_token") String access_token);
//    @GET("own_list/my_answer/search/{search}/offset/{offset}/{login}/{access_token}")
//    Call<ArrayList<OwnList>> searchMyAnswers(@Path("search") String search, @Path("offset") int offset, @Path("login") String login,
//                                             @Path("access_token") String access_token);
//    @GET("own_list/my_answer/uid/{uid}/offset/{offset}/{login}/{access_token}")
//    Call<ArrayList<OwnList>> getAnswerOfId(@Path("uid") int uid, @Path("offset") int offset, @Path("login") String login,
//                                           @Path("access_token") String access_token);
//    @GET("own_list/my_answer/uid/{uid}/search/{search}/offset/{offset}/{login}/{access_token}")
//    Call<ArrayList<OwnList>> searchAnswerOfId(@Path("uid") int uid, @Path("search") String search, @Path("offset") int offset, @Path("login") String login,
//                                              @Path("access_token") String access_token);
//    @Headers({ "Content-Type: application/json-rpc" ,"Connection:close"})
//    @POST("word/love/new/{login}/{access_token}")
//    Call<ResponseBody> setLove(
//            @Path("login") String login,
//            @Path("access_token") String access_token,
//            @Body JsonObject body);
//    @Headers({ "Content-Type: application/json-rpc" ,"Connection:close"})
//    @POST("user/follow/new/{login}/{access_token}")
//    Call<ResponseBody> follow(
//            @Path("login") String login,
//            @Path("access_token") String access_token,
//            @Body JsonObject body);
//    @Headers({ "Content-Type: application/json-rpc" ,"Connection:close"})
//    @POST("user/unfollow/{login}/{access_token}")
//    Call<ResponseBody> unfollow(
//            @Path("login") String login,
//            @Path("access_token") String access_token,
//            @Body JsonObject body);
//    @GET("user/follower/uid/{uid}/offset/{offset}/{login}/{access_token}")
//    Call<ArrayList<User>> getFollowerOfId(@Path("uid") int uid, @Path("offset") int offset, @Path("login") String login,
//                                          @Path("access_token") String access_token);
//    @GET("user/follower/uid/{uid}/search/{search}/offset/{offset}/{login}/{access_token}")
//    Call<ArrayList<User>> searchFollowerOfId(@Path("uid") int uid, @Path("search") String search, @Path("offset") int offset, @Path("login") String login,
//                                             @Path("access_token") String access_token);
//    @GET("user/follower/offset/{offset}/{login}/{access_token}")
//    Call<ArrayList<User>> getFollower(@Path("offset") int offset, @Path("login") String login,
//                                      @Path("access_token") String access_token);
//    @GET("user/follower/search/{search}/offset/{offset}/{login}/{access_token}")
//    Call<ArrayList<User>> searchFollower(@Path("search") String search, @Path("offset") int offset, @Path("login") String login,
//                                         @Path("access_token") String access_token);
//    @GET("user/following/offset/{offset}/{login}/{access_token}")
//    Call<ArrayList<User>> getFollowing(@Path("offset") int offset, @Path("login") String login,
//                                       @Path("access_token") String access_token);
//    @GET("user/following/search/{search}/offset/{offset}/{login}/{access_token}")
//    Call<ArrayList<User>> searchFollowing(@Path("search") String search, @Path("offset") int offset, @Path("login") String login,
//                                          @Path("access_token") String access_token);
//
//    @Headers({ "Content-Type: application/json-rpc" ,"Connection:close"})
//    @POST("word/reject/{login}/{access_token}")
//    Call<ResponseBody> setReject(
//            @Path("login") String login,
//            @Path("access_token") String access_token,
//            @Body JsonObject body);
//
//    @Headers({ "Content-Type: application/json-rpc" ,"Connection:close"})
//    @POST("user/set_phone/{login}/{access_token}")
//    Call<ResponseBody> setPhone(
//            @Path("login") String login,
//            @Path("access_token") String access_token,
//            @Body JsonObject body);
//
//    @Headers({ "Content-Type: application/json-rpc" ,"Connection:close"})
//    @POST("testimony/new/{login}/{access_token}")
//    Call<ResponseBody> createTestimony(
//            @Path("login") String login,
//            @Path("access_token") String access_token,
//            @Body JsonObject body);
//    @Multipart
//    @POST("user/set_selfie_picture/{login}/{access_token}")
//    Call<ResponseBody> setSelfiePicture(
//            @Path("login") String login,
//            @Path("access_token") String access_token,
//            @Part MultipartBody.Part file);
//    @Multipart
//    @POST("user/set_identity_picture/{login}/{access_token}")
//    Call<ResponseBody> setIdPicture(
//            @Path("login") String login,
//            @Path("access_token") String access_token,
//            @Part MultipartBody.Part file);
//
//    @Headers({ "Content-Type: application/json-rpc" ,"Connection:close"})
//    @POST("user/set_verified_account/{login}/{access_token}")
//    Call<ResponseBody> setAccountVerified(
//            @Path("login") String login,
//            @Path("access_token") String access_token,
//            @Body JsonObject body);
//
//    @Headers({ "Content-Type: application/json-rpc" ,"Connection:close"})
//    @POST("user/pull_fund/{login}/{access_token}")
//    Call<ResponseBody> pullFund(
//            @Path("login") String login,
//            @Path("access_token") String access_token,
//            @Body JsonObject body);
//
//    @Headers({ "Content-Type: application/json-rpc" ,"Connection:close"})
//    @POST("user/bank_account/new/{login}/{access_token}")
//    Call<ResponseBody> setBankAccount(
//            @Path("login") String login,
//            @Path("access_token") String access_token,
//            @Body JsonObject body);
}