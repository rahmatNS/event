package xyz.rahmatnasution.event;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

public class BottomNavigation extends AppCompatActivity {


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            FragmentManager fragmentManager= getSupportFragmentManager();
            FragmentTransaction transaction= fragmentManager.beginTransaction();
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    return true;
                case R.id.navigation_search:
                    return true;

                case R.id.navigation_account:
                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_navigation);


        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    public void company(View view) {
        Intent company = new Intent(this, CompanyProfile.class);
        startActivity(company);
    }

    public void product(View view) {
        Intent company = new Intent(this, ProductActivity.class);
        startActivity(company);
    }
    public void business(View view) {
        Intent company = new Intent(this, BussinesPlane.class);
        startActivity(company);
    }
    public void test(View view) {
        Intent company = new Intent(this, Gallery.class);
        startActivity(company);
    }
    public void event(View view) {
        Intent company = new Intent(this, EventActivity.class);
        startActivity(company);
    }
    public void news(View view) {
        Intent company = new Intent(this, News.class);
        startActivity(company);
    }

}
