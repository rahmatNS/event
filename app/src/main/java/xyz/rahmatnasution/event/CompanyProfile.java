package xyz.rahmatnasution.event;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xyz.rahmatnasution.event.Api.ApiClient;
import xyz.rahmatnasution.event.Api.ApiInterface;

public class CompanyProfile extends AppCompatActivity {

    public static final String TAG = CompanyProfile.class.getSimpleName();
    private Toolbar toolbar;
    ContentLoadingProgressBar contentLoadingProgressBar;
    TextView companyProfileTextView;
    ApiInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_profile);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Company Profile");

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        contentLoadingProgressBar = (ContentLoadingProgressBar) findViewById(R.id.contentLoadingProgressBar);
        companyProfileTextView = (TextView) findViewById(R.id.company_profile_text_view);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        getCompanyProfileContent();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }
    void getCompanyProfileContent(){
        apiInterface.getCompanyProfile().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                contentLoadingProgressBar.hide();
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    JSONObject companyProfile = jsonObject.getJSONObject("company_profile");
                    JSONArray columns = companyProfile.getJSONArray("columns");
                    JSONArray records = companyProfile.getJSONArray("records");
                    for (int i = 0; i < records.length(); i++){
                        JSONArray record = records.getJSONArray(i);
                        for (int j = 0; j < record.length(); j++){
                            if(columns.getString(j).equals("content")){
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                                    companyProfileTextView.setText(Html.fromHtml(record.getString(j),Html.FROM_HTML_MODE_LEGACY,imageGetter,null));
                                    companyProfileTextView.setMovementMethod(LinkMovementMethod.getInstance());
                                } else {
                                    companyProfileTextView.setText(Html.fromHtml(record.getString(j),imageGetter,null));
                                    companyProfileTextView.setMovementMethod(LinkMovementMethod.getInstance());
                                }
                            }
                        }
                    }

                }catch (Exception e){
                    Toast.makeText(CompanyProfile.this, "Error occured on getting data", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "onResponse: e = "+e.fillInStackTrace());
                    Log.d(TAG, "onResponse: response.raw() = "+response.raw());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                contentLoadingProgressBar.hide();
                Toast.makeText(CompanyProfile.this, "Failed to get data", Toast.LENGTH_SHORT).show();
                Log.d(TAG, "onFailure: t = "+t.fillInStackTrace());
            }
        });
    }
    Html.ImageGetter imageGetter = new Html.ImageGetter() {
        @Override
        public Drawable getDrawable(String source) {
            Drawable drawable;
            int sourceId =
                    getApplicationContext()
                            .getResources()
                            .getIdentifier(source, "drawable", getPackageName());
            try {
                HttpURLConnection connection = (HttpURLConnection)new URL(source) .openConnection();
                connection.setRequestProperty("User-agent","Mozilla/4.0");

                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap bitmap =  BitmapFactory.decodeStream(input);
                drawable = new BitmapDrawable(getResources(),bitmap);
                drawable.setBounds(
                        0,
                        0,
                        drawable.getIntrinsicWidth(),
                        drawable.getIntrinsicHeight());
                return drawable;
            }catch (Exception e){
                Log.d(TAG, "getDrawable: e = "+e.fillInStackTrace());
                return null;
            }
        }
    };
}
