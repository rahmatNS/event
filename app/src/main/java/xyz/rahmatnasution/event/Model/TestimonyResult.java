package xyz.rahmatnasution.event.Model;

import java.util.List;

/**
 * Created by rahma on 19/09/2017.
 */

public class TestimonyResult {
    TestimonyData testimony;

    public TestimonyData getTestimony() {
        return testimony;
    }

    public void setTestimony(TestimonyData testimony) {
        this.testimony = testimony;
    }
    public List<Testimony> getTestimonies(){
        return getTestimony().getRecords();
    }

}

