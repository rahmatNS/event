package xyz.rahmatnasution.event.Model;

import java.util.List;

/**
 * Created by rahma on 19/09/2017.
 */
public class TestimonyData {
    String[] columns;
    List<Testimony> records;

    public String[] getColumns() {
        return columns;
    }

    public void setColumns(String[] columns) {
        this.columns = columns;
    }

    public List<Testimony> getRecords() {
        return records;
    }

    public void setRecords(List<Testimony> records) {
        this.records = records;
    }
}
