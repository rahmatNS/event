package xyz.rahmatnasution.event.Model;

/**
 * Created by rahma on 19/09/2017.
 */

public class Testimony {
    int id;
    String foto;
    String nama;
    String keterangan_nama;
    String testimoni;
    int sort_id;

    public Testimony(){

    }

    public Testimony(int id, String foto, String nama, String keterangan_nama, String testimoni, int sort_id){
        super();
        this.id = id;
        this.foto = foto;
        this.nama = nama;
        this.keterangan_nama = keterangan_nama;
        this.testimoni = testimoni;
        this.sort_id = sort_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKeterangan_nama() {
        return keterangan_nama;
    }

    public void setKeterangan_nama(String keterangan_nama) {
        this.keterangan_nama = keterangan_nama;
    }

    public String getTestimoni() {
        return testimoni;
    }

    public void setTestimoni(String testimoni) {
        this.testimoni = testimoni;
    }

    public int getSort_id() {
        return sort_id;
    }

    public void setSort_id(int sort_id) {
        this.sort_id = sort_id;
    }
}
