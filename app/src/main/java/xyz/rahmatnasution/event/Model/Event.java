package xyz.rahmatnasution.event.Model;

import xyz.rahmatnasution.event.Helper.EventHelper;

/**
 * Created by rahma on 01/10/2017.
 */

public class Event {
    int id = 0;
    String type = "";
    String venue = "";
    String image = "";
    String latitude = "";
    String longitude = "";
    String from_timestamp = "";
    String to_timestamp = "";
    String target = "";
    String admission = "";
    String price = "";
    String price_discount = "";
    String title = "";
    String title_description = "";
    String description = "";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getImage() {
        return image;
    }

    public String getImageURL() {
        return EventHelper.getImageUrl(image);
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getFrom_timestamp() {
        return from_timestamp;
    }

    public void setFrom_timestamp(String from_timestamp) {
        this.from_timestamp = from_timestamp;
    }

    public String getTo_timestamp() {
        return to_timestamp;
    }

    public void setTo_timestamp(String to_timestamp) {
        this.to_timestamp = to_timestamp;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getAdmission() {
        return admission;
    }

    public void setAdmission(String admission) {
        this.admission = admission;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice_discount() {
        return price_discount;
    }

    public void setPrice_discount(String price_discount) {
        this.price_discount = price_discount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle_description() {
        return title_description;
    }

    public void setTitle_description(String title_description) {
        this.title_description = title_description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
