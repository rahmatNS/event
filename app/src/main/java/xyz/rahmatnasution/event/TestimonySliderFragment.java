package xyz.rahmatnasution.event;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;

import xyz.rahmatnasution.event.Helper.CircleTransform;
import xyz.rahmatnasution.event.Helper.EventHelper;
import xyz.rahmatnasution.event.Model.Testimony;


public class TestimonySliderFragment extends Fragment {
    private static final String TAG = TestimonySliderFragment.class.getSimpleName();
    private Testimony testimony;
    ImageView imageView;
    TextView testimonyTextView;
    TextView nameTextView;

    public TestimonySliderFragment() {
    }

    public static TestimonySliderFragment newInstance(Testimony testimony) {
        TestimonySliderFragment fragment = new TestimonySliderFragment();
        fragment.testimony = testimony;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_slider, container, false);
        imageView = (ImageView) view.findViewById(R.id.image);
        testimonyTextView = (TextView) view.findViewById(R.id.testimony);
        nameTextView = (TextView) view.findViewById(R.id.name);

        CircleCrop circleCrop = new CircleCrop();

        Glide.with(this)
                .load(EventHelper.getImageUrl(testimony.getFoto()))
                .apply(RequestOptions.circleCropTransform())
//                .apply(RequestOptions.bitmapTransform(new CircleTransform()))
                .into(imageView);
        testimonyTextView.setText(testimony.getTestimoni());
        nameTextView.setText(testimony.getNama());
        return view;
    }
}
