package xyz.rahmatnasution.event;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import xyz.rahmatnasution.event.Helper.UserSession;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UserSession.getInstance().setActivity(this);
        checkLogin();
    }
    private void checkLogin(){
        Intent intent;
        if(UserSession.getInstance().isLoggedIn()){
            intent = new Intent(SplashActivity.this,MenuActivity.class);
        }else {
            intent = new Intent(SplashActivity.this,LoginActivity.class);
        }
        startActivity(intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkLogin();
    }
}
